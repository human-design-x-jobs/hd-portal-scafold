/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./components/**/*.{js,vue,ts}",
    "./layouts/**/*.vue",
    "./pages/**/*.vue",
    "./plugins/**/*.{js,ts}",
    "./app.vue",
  ],

  theme: {
    screens: {
      xs: "360px",
      // => @media (min-width: 640px) { ... }

      sm: "640px",
      // => @media (min-width: 640px) { ... }

      md: "960px",
      // => @media (min-width: 768px) { ... }

      lg: "1200px",
      // => @media (min-width: 1024px) { ... }

      xl: "1600px",
      // => @media (min-width: 1280px) { ... }

      "2xl": "1920px",
      // => @media (min-width: 1536px) { ... }
    },
    extend: {
      fontFamily: {
        poppins: [
          "Poppins",
          "system-ui",
          "-apple-system",
          "BlinkMacSystemFont",
          "Segoe UI",
          "Roboto",
          "Oxygen",
          "Ubuntu",
          "Cantarell",
          "Open Sans",
          "Helvetica Neue",
          "sans-serif",
        ],
        roboto: ["Helvetica Neue", "Roboto", "Open Sans", "sans-serif"],
      },
      aspectRatio: {
        "16/9": "16 / 9",
      },
      colors: {
        xgray: {
          "0c": "#0c0c0c",
          "0f": "#0f0f0f",
          13: "#131313",
          14: "#141414",
          19: "#191919",
          "1a": "#1a1a1a",
          "1e": "#1e1e1e",
          20: "#202020",
          "2d": "#2d2d2d",
          30: "#303030",
          31: "#313131",
          "3d": "#3d3d3d",
          40: "#404040",
          "5c": "#5c5c5c",
          69: "#696969",
          "6a": "#6a6a6a",
          99: "#999999",
          c9: "#c9c9c9",
          e7: "#e7e7e7",
          de: "#dedede",
        },
        main: {
          purple: "#713FD0",
        },
        link: {
          gold: "#FFBC40",
          blue: "#3EC6FF",
        },
        error: "#E34716",
      },
      fontSize: {
        h1: ["60px", "64px"],
        "h1-mobile": ["44px", "46px"],
        h2: ["36px", "40px"],
        "h2-mobile": ["32px", "36px"],
        h3: ["24px", "26px"],
        "h3-mobile": ["20px", "24px"],
        "landing-main": ["18px", "28px"],
        h4: ["18px", "20px"],
        "h4-mobile": ["16px", "18px"],
        main: ["16px", "28px"],
        "main-mobile": ["14px", "22px"],
        description: ["14px", "20px"],
        "description-mobile": ["13px", "16px"],
        btn: ["14px", "16px"],
        "nav-link": ["16px", "18px"],
        input: ["16px", "18px"],
        tag: ["12px", "14px"],
      },
      height: {
        lh: "1lh",
        em: "1em",
      },
    },
  },
  plugins: [],
};
