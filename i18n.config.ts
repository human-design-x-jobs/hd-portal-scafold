export default defineI18nConfig(() => ({
  legacy: false,
  locales: [
    {
      code: "en",
      file: "en-US.ts",
    },
    {
      code: "ru",
      file: "ru-RU.ts",
    },
  ],
  langDir: "locales",
}));
