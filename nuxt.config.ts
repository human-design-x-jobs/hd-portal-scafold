// ./nuxt.config.ts
// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  css: ["~/assets/css/main.css"],
  app: {
    head: {
      viewport:
        "width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no",
    },
  },
  router: {
    options: {
      scrollBehaviorType: "smooth",
    },
  },
  pwa: {
    /* PWA options */
    manifest: {
      name: "Human Design X",
      short_name: "Human Design X",
      description: "Human Design X",
      theme_color: "#0c0c0c",
      icons: [
        {
          src: "pwa-192x192.png",
          sizes: "192x192",
          type: "image/png",
        },
        {
          src: "pwa-512x512.png",
          sizes: "512x512",
          type: "image/png",
        },
        {
          src: "pwa-512x512.png",
          sizes: "512x512",
          type: "image/png",
          purpose: "any",
        },
        {
          src: "pwa-512x512.png",
          sizes: "512x512",
          type: "image/png",
          purpose: "maskable",
        },
      ],
    },
    workbox: {
      navigateFallback: "/", // Fallback to index.html
      globPatterns: [
        "**/*.{js,css,html,png,jpg,jpeg,svg,woff2,woff,ttf,eot,webmanifest}",
      ],
    },
    client: {
      installPrompt: true,
    },
    registerWebManifestInRouteRules: true,
    devOptions: {
      enabled: true,
      navigateFallbackAllowlist: [/^\/$/],
    },
    registerType: "autoUpdate",
  },
  postcss: {
    plugins: {
      "tailwindcss/nesting": {},
      tailwindcss: {},
      autoprefixer: {},
    },
  },
  vite: {
    vue: {
      script: {
        defineModel: true,
        propsDestructure: true,
      },
    },
  },
  i18n: {
    locales: [
      {
        code: "en",
        file: "en-US.ts",
      },
      {
        code: "ru",
        file: "ru-RU.ts",
      },
    ],
    langDir: "locales",
    strategy: "no_prefix",
  },
  modules: [
    "@vueuse/motion/nuxt",
    "@vite-pwa/nuxt",
    "@nuxtjs/device",
    "nuxt-zod-i18n",
    "@nuxtjs/i18n",
    "nuxt-lodash",
    "@pinia/nuxt",
    "@nuxtjs/supabase",
    "nuxt-svgo",
    "nuxt-icon",
    [
      "@nuxtjs/google-fonts",
      {
        families: {
          Poppins: [300, 400, 500, 600],
          Roboto: [300, 400, 500, 600],
        },
      },
    ],
    "@nuxtjs/i18n",
    "@nuxtjs/seo",
    "nuxt-route-meta",
    "nuxt-zod-i18n",
  ],
  typescript: {
    shim: false,
  },
  supabase: {
    redirect: false,
  },
  device: {
    refreshOnResize: true,
  },
});
